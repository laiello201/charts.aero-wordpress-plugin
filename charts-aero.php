<?php
/**
 * Plugin Name: Charts.Aero Wordpress Plugin
 * Plugin URI: http://projects.lorenzoaiello.com/charts-wp-plugin/
 * Description: A simple plugin that allows embedding of airport charts using the Charts.Aero Developer API. After saving the API Key, use "[charts icao=XXXX]" or "[charts iata=XXX]" and the charts will automatically be loaded.
 * Version: 1.0
 * Author: Lorenzo Aiello
 * Author URI: http://lorenzoaiello.com
 * License: GPL2
 */
 
 require_once('Unirest.php');
 
 add_shortcode("charts", "charts_loader");
 
 function charts_loader($params)
 {
 	$options = get_option('plugin_options');
 	$api_key = $options['charts_api_key'];
 
 	if($api_key == '')
 	{
 		return 'You must enter an API Key to load airport charts.';
 	}
 
 	if(isset($params['icao']) && $params['icao'] != '')
 	{
 		$response = Unirest::post(
		  "https://charts.p.mashape.com/retrieve/icao",
		  array(
		    "X-Mashape-Authorization" => $api_key,
		  ),
		  array(
		    "icao" => $params['icao']
		  )
		);
 	}else{
 		$response = Unirest::post(
		  "https://charts.p.mashape.com/retrieve/iata",
		  array(
		    "X-Mashape-Authorization" => $api_key, //"h9cido6xmnpwbc0qgc972nggeabmt3"
		  ),
		  array(
		    "iata" => $params['iata']
		  )
		);
 	}
 	
 	$response = array_values((array)$response);
 	$response = $response[2];
 	
 	if($response->result == 'SUCCESS')
 	{
 		$html = '<table>';
 		$html .= '<tr>
 				<th>'.$response->airport->airport_name.' ('.$response->airport->icao.' / '.$response->airport->iata.')</th>
 			</tr>';
 		foreach($response->data as $chart)
 		{
 			$html .= '<tr>
 					<td><a href="'.$chart->file_url.'" target="_blank">'.$chart->chart_name.'</a></td>
 				</tr>';
 		}
 		$html .= '</table>';
 		
 		return $html;
 	}else{
 		return "An error occured loading airport charts.";
 	}
 }
 
 add_action('admin_menu', 'chart_settings');
 
 add_action('admin_init', 'plugin_admin_init');
 function plugin_admin_init()
 {
	register_setting( 'plugin_options', 'plugin_options', 'plugin_options_validate' );
	add_settings_section('plugin_main', 'API Settings', 'plugin_section_text', 'plugin');
	add_settings_field('charts_api_key', 'API Key', 'plugin_setting_string', 'plugin', 'plugin_main');
 }
 
 function plugin_section_text()
 {
	echo '<p>You must specify an API Key to use this plugin. If you do not already have one, you get get one here: <a href="https://www.mashape.com/laiello/airport-charts" target="_blank">https://www.mashape.com/laiello/airport-charts</a>.</p>';
 }
 
 function plugin_options_validate($input)
 {
	$options = get_option('plugin_options');
	$options['charts_api_key'] = trim($input['charts_api_key']);
	return $options;
 }
 
 function plugin_setting_string()
 {
	$options = get_option('plugin_options');
	echo "<input id='charts_api_key' name='plugin_options[charts_api_key]' size='40' type='text' value='{$options['charts_api_key']}' />";
 }
 
 function chart_settings()
 {
 	add_options_page('Charts.Aero API Key', 'Charts.Aero API Key', 'manage_options', 'plugin', 'plugin_options_page');
 }
 
 function plugin_options_page()
 {
 ?>

	<div>
	<h2>Charts.Aero API Key</h2>
	<form action="options.php" method="post">
	<?php settings_fields('plugin_options'); ?>
	<?php do_settings_sections('plugin'); ?>
	 
	<input name="Submit" type="submit" value="<?php esc_attr_e('Save Changes'); ?>" />
	</form></div>
 
 <?php } ?>